﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RepetGameController : MonoBehaviour {

	public  int cantidadRepetir;
	private static int vueltaActual;
	public static bool siguiente;
	public static RepetGameController instance;
	public static bool resetGame;
	public GameObject game;
	public string NombreScript;
	void Awake(){
		instance = this;
	}
	void Start () {
		RepetGameController.vueltaActual = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public static void darVuelta(){
		if(instance.cantidadRepetir != RepetGameController.vueltaActual){
			// reiniciar game;
			//Debug.Log("instacia un game " + instance.game.GetComponent<ComparacionManger3Atributos>().resetElementos());
			
			//ComparacionManger3Atributos.resetElementos();
			
		} else {
			if (siguiente){
				instance.StartCoroutine("mostrarSiguiente");
			} else {
				instance.StartCoroutine("mostrarCompletada");
				//cargar resultados
			}
		}
		RepetGameController.vueltaActual++;
	}

	IEnumerator mostrarSiguiente(){
		yield return new WaitForSeconds(2);
		//activar siguiente para que aparesca botón
	}
	IEnumerator mostrarCompletada(){
		yield return new WaitForSeconds(1);
		SceneManager.LoadScene("Completada");
	}

}

