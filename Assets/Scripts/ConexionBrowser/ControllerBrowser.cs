﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using UnityEngine.UI;

public class ControllerBrowser : MonoBehaviour {

	// Use this for initialization
	public Text nombre;
	[DllImport("__Internal")]
    private static extern void SendMessageToWeb(string msg);
    [DllImport("__Internal")]
    private static extern void HelloString(string str);

    public void ReceiveMessageFromWeb(string msg) {
        Debug.Log("Controller.ReceiveMessageFromWeb: " + msg);
        nombre.text = msg;
    }
    // Use this for initialization
    void Start() {
        //SendMessageToWeb("Hello from Unity");

    }

	// Update is called once per frame
	void Update() {

	}
	public void enviarString(){
		HelloString("This is a string.");
	}
	
}
