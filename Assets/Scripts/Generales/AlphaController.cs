﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlphaController : MonoBehaviour {

	// Use this for initialization
	//public GameObject imagen;
	void Start () {
		//GetComponent<SpriteRender> ().imagen = imagen;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void disminuirAlpha(){

		Color tmp = gameObject.GetComponent<SpriteRenderer>().color;
		tmp.a = 0.7f;
		gameObject.GetComponent<SpriteRenderer>().color = tmp;
	}
	public void resetAlpha(){
		Color tmp = gameObject.GetComponent<SpriteRenderer>().color;
		tmp.a = 1.0f;
		gameObject.GetComponent<SpriteRenderer>().color = tmp;
	}
}
