﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class CamaraController : MonoBehaviour {

	// Use this for initialization
	private Vector3[] posiciones = new Vector3[]{
		new Vector3 (152.2f, 0.7f, 120.4f),
		new Vector3 (152.1f, 0.5f, 119.1f),
		new Vector3 (150.1f, 0.6f, 120.1f),
		new Vector3 (155.6f, 2.1f, 120.1f),
		//new Vector3 (153.2f, 0.8f, 120.1f),
		new Vector3 (150.9f, 1.6f, 117.9f),
		//
		new Vector3 (155.2f, 1.3f, 117.7f),
		new Vector3 (150.0f, 0.6f, 120.9f),
		new Vector3 (156.0f, 1.7f, 119.2f),
		new Vector3 (151.2f, 0.2f, 117.6f),
		new Vector3 (151.6f, 0.2f, 117.6f),
		new Vector3 (150.2f, 1.4f, 119.3f),
		new Vector3 (155.6f, 2.2f, 118.7f)
		
	};
	public GameObject[] elementos = new GameObject[1] ;

	private float anguloRotacion;
	private float anguloRotacionX;
	private float rotacionY = 0f;
	public GameObject[] particulas = new GameObject[6];
	public GameObject panel;

	private bool estatico = true;
	/*private Vector3[] posicionesRotacion = new Vector3[]{
		(Vector3.right * 1),(Vector3.left * 1)
	};*/
	private string[] nombresActividades = new string[]{
     "Adelante o atrás", "¿Iguales o diferentes?", "más de un atributo", "¿Qué hice ayer?",
    "Números hasta 20", "Secuencias hasta 20", "Patrones", "Resolviendo problemas",
    "Figuras geometrias", "Cuerpos geometrios", "Numeros graficamente", "Suma y resta"
  };
  private int[] ecsenas = new int[]{
  	2, 7, 
  };
	private int numeroPosicion;
	private int numeroPosicionAnterior;
	public GameObject texto;
	public TextMeshProUGUI textoMesh;
	private int vueltaEscena;

	void Start () {
		numeroPosicion = 0;
		numeroPosicionAnterior = 11;
		anguloRotacion = 0f;
		anguloRotacionX = 0f;
		vueltaEscena = -1;
	}

	void Update () {
		//Debug.Log(transform.position.x);
		if(estatico) {
			if (Input.GetKeyDown(KeyCode.RightArrow))
	        {
	          cambiarPosicionCamaraDerecha();
	          
	          
	        }
	    if (Input.GetKeyDown(KeyCode.LeftArrow))
	        {
	          cambiarPosicionCamaraIzquierda();
	          

	        }
	    if (Input.GetKeyDown("space"))
	        {
	          cargarEscena();
	        }
		}
		
	}
	private void cargarEscena(){
		SceneManager.LoadScene(ecsenas[numeroPosicion]);
	}
	public void cambiarPosicionCamaraIzquierda(){
		if (estatico){
			estatico = false;
			numeroPosicionAnterior = numeroPosicion;
			panel.SetActive(false);
			if(numeroPosicion==0){
				numeroPosicion = 11;
			}else{
				numeroPosicion--;
			}
			StartCoroutine("moverCamara");
		}
		
		//activar particulas
		//particulas[numeroPosicion].SetActive(true);
	}
	public void cambiarPosicionCamaraDerecha(){
		
		if(estatico){
			estatico = false;
			numeroPosicionAnterior = numeroPosicion;
			panel.SetActive(false);
			if(numeroPosicion==11){
				numeroPosicion = 0;
			}else{
				numeroPosicion++;
			}
			StartCoroutine("moverCamara");
		}
		
	}
	IEnumerator girarCamaraX(){
		if( anguloRotacionX!=30){
			gameObject.transform.Rotate(3f, 0f, 0f);
			//Debug.Log(anguloRotacionX);
			anguloRotacionX = anguloRotacionX+3;
			yield return new WaitForSeconds(0.0001f);
			StartCoroutine("girarCamaraX");
		}else{
			gameObject.transform.Rotate(0f, 0f, 0f);
			anguloRotacionX = 0;
			Debug.Log("entro en girar camarax");
		}
	}
	IEnumerator girarCamaraXNormalizar(){
		if( anguloRotacionX!=30){
			Debug.Log("anguloRotacionX "+anguloRotacionX);
			gameObject.transform.Rotate(3f, 0f, 0f);
			anguloRotacionX = anguloRotacionX+3;
			yield return new WaitForSeconds(0.0001f);
			StartCoroutine("girarCamaraXNormalizar");
			//estatico = true;
		}else{
			gameObject.transform.Rotate(0f, 0f, 0f);
			anguloRotacionX = 0;
			Debug.Log("entro en girar normalizarcamara");
		}
	}
	IEnumerator girarCamara(){
		if(anguloRotacion !=  180 ){
			gameObject.transform.Rotate(0f, 3f, 0f);
			anguloRotacion = anguloRotacion+3;
			yield return new WaitForSeconds(0.0001f);
			StartCoroutine("girarCamara");
		}else{
			gameObject.transform.Rotate(0f, 0f, 0f);
			anguloRotacion = 0;
			if(numeroPosicion == 1 && (numeroPosicionAnterior == 0 || numeroPosicionAnterior == 2) ){
				StartCoroutine("girarCamaraX");
			}else if((numeroPosicion == 2 || numeroPosicion == 0) && numeroPosicionAnterior==1){
				StartCoroutine("girarCamaraXNormalizar");
			}
			panel.SetActive(true);
			estatico = true;
			textoMesh.text = nombresActividades[numeroPosicion].ToString();
			/*float aux = 0.2f;
			Color tren = elementos[0].GetComponent<Renderer>().material.color;
			tren.a = aux;
			elementos[0].GetComponent<Renderer>().material.color = tren;
			Debug.Log(tren);
			*/
		}
	}

	IEnumerator moverCamara(){
		if(gameObject.transform.position != posiciones[numeroPosicion]){
			float posicionX = gameObject.transform.position.x;	
			float posicionY = gameObject.transform.position.y;
			float posicionZ = gameObject.transform.position.z;
			textoMesh.text = "";
			if( posicionX-0.01 < posiciones[numeroPosicion].x  && posiciones[numeroPosicion].x< posicionX+0.01){
				posicionX = posiciones[numeroPosicion].x;
			}else{
				if(posiciones[numeroPosicion].x>posicionX){
					posicionX = (posicionX+0.1f);
				}else{
					posicionX = (posicionX-0.1f);
				}
			}
			if(posicionY-0.01 <posiciones[numeroPosicion].y && posiciones[numeroPosicion].y < posicionY+0.01){
				posicionY = posiciones[numeroPosicion].y;
			}else{
				if(posiciones[numeroPosicion].y>posicionY){
					posicionY = (posicionY+0.1f);
				}else{
					posicionY = (posicionY-0.1f);
				}	
			}
			if(posicionZ-0.01 < posiciones[numeroPosicion].z && posiciones[numeroPosicion].z < posicionZ+0.01){
				posicionZ = posiciones[numeroPosicion].z;		
			}else{
				if(posiciones[numeroPosicion].z>posicionZ){
					posicionZ = (posicionZ+0.1f);
				}else{
					posicionZ = (posicionZ-0.1f);
				}	
			}
			gameObject.transform.position = new Vector3 (posicionX, posicionY, posicionZ);
			yield return new WaitForSeconds(0.001f);
			StartCoroutine("moverCamara");
		}else{
			StartCoroutine("girarCamara");

		}
		
	}
}
