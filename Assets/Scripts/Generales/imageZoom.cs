﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class imageZoom : MonoBehaviour {

	// Use this for initialization
	//public GameObject imagen;
	void Start () {
		//IniciarZoom();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void IniciarZoom(){
		StartCoroutine("aumentarEscala");
	}
	IEnumerator disminuirEscala(){
			if(gameObject.transform.localScale.x>0.35f){
				transform.localScale -= new Vector3(0.01f, 0.01f, 0);
				yield return new WaitForSeconds(0.01f);
				StartCoroutine("disminuirEscala");
			}else{
				 StartCoroutine("aumentarEscala");
			}
	}
	IEnumerator aumentarEscala(){
		if(gameObject.transform.localScale.x<0.45f){
				gameObject.transform.localScale += new Vector3(0.01f, 0.01f, 0);
				yield return new WaitForSeconds(0.01f);
				StartCoroutine("aumentarEscala");	
			}else{
				StartCoroutine("disminuirEscala");
			}
	}
}
