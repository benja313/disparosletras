﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageAlpha : MonoBehaviour {

	public GameObject imagen;
	private float alpha;
	public bool aumentar;
	

	void Awake(){
		alpha = 0.0f;
	}

	void Start () {
		if(aumentar){
			StartCoroutine("aumentarAlpha");	
		}else{
			StartCoroutine("disminuirAlpha");
		}	
	}
	public void establecerCeroAlpha(){
		alpha = 0.0f;
		StartCoroutine("aumentarAlpha");
	}
	IEnumerator disminuirAlpha(){
			if(alpha<1.1&&alpha>0){
				alpha = alpha-0.1f;
				Color tmp = imagen.GetComponent<Image>().color;
 				tmp.a = alpha;
				imagen.GetComponent<Image>().color = tmp;
				yield return new WaitForSeconds(0.11f);
				StartCoroutine("disminuirAlpha");
			}else{
				 StartCoroutine("aumentarAlpha");
			}
	}
	IEnumerator aumentarAlpha(){
		if(alpha<1&&alpha>-0.1){
				alpha=alpha+0.1f;
				Color tmp = imagen.GetComponent<Image>().color;
 				tmp.a = alpha;
				imagen.GetComponent<Image>().color = tmp;
				yield return new WaitForSeconds(0.1f);
				StartCoroutine("aumentarAlpha");	
			}else{
				//StartCoroutine("disminuirAlpha");
			}
	}

}
