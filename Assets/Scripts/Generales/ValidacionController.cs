﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ValidacionController : MonoBehaviour {

	// Use this for initialization
	public GameObject imagenCorrecto;
	public ImageAlpha correctoAlpha;
	public GameObject imagenIncorrecto;
	public ImageAlpha incorrectoAlpha;
	public static ValidacionController instance;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	 void Awake()
 {
     instance = this;
 }
	public static void cargarResultadoImage(bool opcion){
		if (opcion) {
			instance.StartCoroutine("mostrarCorrecto");
			//Debug.Log("llego verdadero");
			}else{
			instance.StartCoroutine("mostrarIncorrecto");
			//Debug.Log("llego falso");
			}
	}

	IEnumerator mostrarCorrecto(){
		imagenCorrecto.SetActive(true);
		imagenCorrecto.GetComponent<AudioSource>().Play();
		correctoAlpha.establecerCeroAlpha();
		yield return new WaitForSeconds(2);
		imagenCorrecto.SetActive(false);
	}
	IEnumerator mostrarIncorrecto(){
		imagenIncorrecto.SetActive(true);
		imagenIncorrecto.GetComponent<AudioSource>().Play();
		incorrectoAlpha.establecerCeroAlpha();
		yield return new WaitForSeconds(2);
		imagenIncorrecto.SetActive(false);
	}
}
