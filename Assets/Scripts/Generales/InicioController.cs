﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class InicioController:MonoBehaviour {
	public string nombre;
	public string estudiante_id;
	public string sexo;
	public string bloque_id;
	public GameObject botonPlay;
	// Use this for initialization
	void Start () {	
		//setVidas();
	}
	
	// Update is called once per frame
	void Update () {	
	}
	
	private void setNombreEstudiante(string nombre){
		ProjectVars.Instance.nombreEstudiante = nombre;
	}
	private void setVidas(){
		ProjectVars.Instance.vidas = 3;
	}
	private void setEstudiante_id(string estudiante_id){
		ProjectVars.Instance.estudiante_id = int.Parse(estudiante_id);
	}
	private void setBloque_id(string bloque_id){
		ProjectVars.Instance.bloque_id = int.Parse(bloque_id);
	}
	public void setsexoEstudiante(string sexoEstudiante){
		ProjectVars.Instance.sexoEstudiante = sexoEstudiante;
	}

	public void obtenerValores(string json){
		InicioController myObject = new InicioController();
		JsonUtility.FromJsonOverwrite(json, myObject);
		/*setNombreEstudiante(myObject.nombre);
		setEstudiante_id(myObject.estudiante_id);
		setsexoEstudiante(myObject.sexo);
		setBloque_id(myObject.bloque_id);
		botonPlay.SetActive(true);*/
		//realizar prueba con todos los valores
	}
	//myObject = JsonUtility.FromJson<MyClass>(json);


}
