﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class ControllerEncabezado : MonoBehaviour {

	// Use this for initialization
	public TextMeshProUGUI tiempo;
	public TextMeshProUGUI nombre;
	public bool showNombre;
	public bool showTiempo;
	public GameObject[] vidasImage = new GameObject[3];
	private int vidas;
	public Conexion conexion;
	void Start () {
		//coins_t.text = ProjectVars.Instance.coin.ToString();
		//Debug.Log(pasaraMinutos());
		ProjectVars.Instance.cantidadErrores=0;
		ProjectVars.Instance.time = 0;
		
	}
	
	// Update is called once per frame
	void Update () {
		ProjectVars.Instance.time += Time.deltaTime;
		//tiempo.text = pasaraMinutos();
		if(showTiempo){
			tiempo.text = pasaraMinutos();	
		}

}
	public void disminuirVidas(){
		if(ProjectVars.Instance.vidas==1){
			Debug.Log("vidas 0");
			ProjectVars.Instance.vidas--;
			conexion.cargarDatos();
			//si las vidas llegan a 0 enviar datos.
			StartCoroutine("cargarEscena");
			}else{
				vidasImage[(ProjectVars.Instance.vidas-1)].SetActive(false);
				ProjectVars.Instance.vidas--;
				ProjectVars.Instance.cantidadErrores++;
			}
	}
	IEnumerator cargarEscena(){
		yield return new WaitForSeconds(0.5f);
		SceneManager.LoadScene(0);
	}
	public string pasaraMinutos() {
		int minutos = 0; 
		int segundos = 0;
		minutos = ((int)ProjectVars.Instance.time)/60;
		segundos = (int)((ProjectVars.Instance.time)%60);
		string tiempo = minutos + " : " + segundos;
		return tiempo;
	}
	public void resetVidas(){
		ProjectVars.Instance.vidas = 3;
		for(int i = 0; i<3;i++){
			vidasImage[i].SetActive(false);
			vidasImage[i].SetActive(true);
			vidasImage[i].GetComponent<imageZoom>().IniciarZoom();
		}
	}
}

