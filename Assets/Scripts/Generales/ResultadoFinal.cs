﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ResultadoFinal : MonoBehaviour {

	public Text timer_t;
	public Text errores_t;
	public Text nombreEstudiante;

	int tiempofinal = 1200;

	// Use this for initialization
	void Start () {
		errores_t.text = ProjectVars.Instance.cantidadErrores.ToString();

		timer_t.text = pasaraMinutos();
		nombreEstudiante.text = ProjectVars.Instance.nombreEstudiante;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private string tiempoOcupado() {
		int resultado = ((int)ProjectVars.Instance.time);
		return resultado.ToString();
	}
	private string pasaraMinutos() {
		int minutos = 0; 
		int segundos = 0;
		minutos = ((int)ProjectVars.Instance.time)/60;
		segundos = (int)((ProjectVars.Instance.time)%60);
		string tiempo = minutos + " : " + segundos;
		return tiempo;
	}
}
