﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class  Conexion : MonoBehaviour {

	// Use this for initialization
    private string actividad_id;
    public static int numero;
    public static int vidas;
    private string estudiante_id;
    private string bloque_id;

	 void Start()
    {
       
    }
    void Update(){

    }

    IEnumerator Upload()
    {
        WWWForm form = new WWWForm();
        form.AddField("actividad_id", ProjectVars.Instance.actividad_id);
        form.AddField("numero", ProjectVars.Instance.numero);//numero de la actividad. Es 0 para el inicio
        form.AddField("vidas", ProjectVars.Instance.vidas);//
        form.AddField("estudiante_id", ProjectVars.Instance.estudiante_id);
        form.AddField("bloque_id", ProjectVars.Instance.bloque_id);
      

        //localhost = 192.168.8.84
        //api/ejercicios
        //
        Debug.Log("intenta cargar datos");
        using (UnityWebRequest www = UnityWebRequest.Post("http://192.168.1.84:8888/CeoSim/AcamyKids/public/api/ejercicios", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                cargarDatos();
            }
            else
            {   
                Debug.Log("llega "+www.downloadHandler.text);
                ProjectVars.Instance.actividad_id = int.Parse(www.downloadHandler.text);
                
            }
        }
    }
    public void cargarDatos(){
        StartCoroutine(Upload());
    }
}
