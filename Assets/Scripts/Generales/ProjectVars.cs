﻿using System.Collections.Generic;
using UnityEngine;

public class ProjectVars : Singleton<ProjectVars> {

	//Variables para instanciar
	public int  coin;
	public float time = 0;
	public int vidas = 0;
	public int cantidadErrores = 0;
	public int numero = 1; //numero de la actividad
	public int estudiante_id = 1;//obtenido del Frontend
	public int bloque_id = 55;//debe estar definido y no es modificado
	public int actividad_id = 0;//debe estar definido y no es modificado
	public string nombreEstudiante = "sin nombre";//obtenido del Frontend
	public string sexoEstudiante;// (para dar mensaje dependiendo de sexo del estudiante)


	public static ProjectVars Instance {
		get {
			return ((ProjectVars)mInstance);
		} set {
			mInstance = value;
		}
	}

	protected ProjectVars () {}
}
