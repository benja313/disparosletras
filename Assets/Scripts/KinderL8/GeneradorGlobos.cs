﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GeneradorGlobos : MonoBehaviour {

	// Use this for initialization

	private string figura;
	private int idFigura;
	public GameObject globo;
	private int cont;
	private int contadorTiempo;
	public Text contadorTiempoText;
	public ManagerDisparoGlobos manager;
	

	void Awake(){
		
	}
	void Start () {
		 
	}
	IEnumerator iniciarGlobo()
    {		
    		contadorTiempoText.text = contadorTiempo.ToString();
        contadorTiempo--;
        if(contadorTiempo>=0){
        	yield return new WaitForSeconds(1);
        	GameObject clone = (GameObject)Instantiate(globo, new Vector3(Random.Range(50,1270), -69.5f, 0f), transform.rotation);
        	clone.transform.parent = gameObject.transform;
        	StartCoroutine("iniciarGlobo");
        }else{
        	manager.resetManagerDisparo();
        	 foreach (Transform child in transform)
         {
             Destroy(child.gameObject);
         }

        	//llamar a managerDisparoGLobos para iniciar otra vez todo;
        }    
    }
  public void iniciarGeneradorGlobos(){
  	this.contadorTiempo = 60;	
  	StartCoroutine("iniciarGlobo");
  }
	// Update is called once per frame

	

}
