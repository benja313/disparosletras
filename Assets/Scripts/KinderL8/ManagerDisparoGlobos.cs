﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ManagerDisparoGlobos : MonoBehaviour {

private int idLetra;
private int contadorGlobosCorrectos;
public Text contadorCorrectosText;
public GameObject[] letras = new GameObject[4];
private int[] posicionesIndicesRonda=new int[3];
private int repetir = 3;
public ControllerEncabezado encabezado;
public GeneradorGlobos generadorGlobos;
private int vueltaActual;
public Conexion conexion;


	void Start () {
		vueltaActual = 0;
		asignarIndicesRonda();
		iniciarManagerDisparo();
	}
	public int getIdLetra(){
		return idLetra;
	}
	private void iniciarManagerDisparo(){
		idLetra = posicionesIndicesRonda[vueltaActual];
		vueltaActual++;
		if(vueltaActual==3){
			idLetra = 3;
		}
		contadorGlobosCorrectos = 0;
		generadorGlobos.iniciarGeneradorGlobos();
		letras[idLetra].SetActive(true);
		Debug.Log(letras[idLetra].name + " " +idLetra+" vuelta: "+vueltaActual);
		encabezado.resetVidas();
	}
	public void resetManagerDisparo(){
		conexion.cargarDatos();
		if(vueltaActual==3){
			StartCoroutine("cargarCompletada");
		}else{
			letras[idLetra].SetActive(false);
			StartCoroutine("iniciarSiguiente");
		}

	}
	IEnumerator cargarCompletada(){
		yield return new WaitForSeconds(2f);
		SceneManager.LoadScene("Completada");
	}
	IEnumerator iniciarSiguiente(){
		ProjectVars.Instance.numero++;
		yield return new WaitForSeconds(4);
		iniciarManagerDisparo();
	}
	public void setContadorCorrectos(){
		contadorGlobosCorrectos++;
		contadorCorrectosText.text = contadorGlobosCorrectos.ToString();
	}
	public void eliminarVida(){
		encabezado.disminuirVidas();
	}

	private void asignarIndicesRonda(){
		for(int i = 0; i<2;i++){
			posicionesIndicesRonda[i] = -1;
			posicionesIndicesRonda[i] = generadorPosicionesRandom();
			//Debug.Log(posicionesIndicesRonda[i]);
		}
	}
	private int generadorPosicionesRandom(){
		int aux = Random.Range(0, 2);
		bool estado = encontrarIgualPosicion(aux);
		if(estado){
			return aux;
		}else{
			return	generadorPosicionesRandom();
		}
	}
	
	private bool encontrarIgualPosicion(int numeroRandom){
		bool estado = true;
		for(int i = 0;i<2;i++){
			//cambio por numero random posicionesIndicesRonda[i], sino entra a bucle infinito
			if((numeroRandom==posicionesIndicesRonda[i]) ){
				estado = false;
				break;
			}
		}
		return estado;
	}
	

}
