﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Globo : MonoBehaviour {

public GameObject[] letras = new GameObject[3];
private int ejeX;
private int ejeY;
private int vocal;
public ManagerDisparoGlobos manager;
public ActivarAudio audio;
public ActivarAudio audioError;


	void Start () {
		
		ejeY = Random.Range(30,70);
		moviminetoEjeX();
		vocal = Random.Range(0,3);
		for (int i = 0; i<3; i++){
			if(i!=vocal){
					letras[i].SetActive(false);
				}else{
					letras[i].SetActive(true);
				}
		}
	}
	void Update()
	{
		transform.Translate(new Vector3(ejeX, ejeY, 0.0f) * Time.deltaTime*2);
	}

	public void moviminetoEjeX(){
		if(gameObject.transform.position.x<683){
		ejeX = Random.Range(0,61);
		}else{
			ejeX = Random.Range(-60,1);
		}
	}
	
	public void Comprobar(){
		
		if(vocal==manager.getIdLetra()){
			manager.setContadorCorrectos();
			audio.play();
			if(gameObject.name=="Globo"){
				gameObject.transform.position = new Vector3(10000f,0f,0f);
			}else{
				Destroy(gameObject);
			}
		}else if(manager.getIdLetra()==3 &&(vocal==0||vocal==1)){
			audio.play();
			Destroy(gameObject);
		} else {
			manager.eliminarVida();
			audioError.play();
		}

	}

}
