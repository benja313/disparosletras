﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(AudioSource))]
public class ItemsEncajarAletras : MonoBehaviour {

	public bool dragging = false;
	public bool collision = false;
	private Vector3 position;
	public EncajarAletras manager;
	public int id;
	Rigidbody2D m_Rigidbody;
	private Vector3 positionInicial;

	void Start () {		
		m_Rigidbody = GetComponent<Rigidbody2D>();
		positionInicial = transform.position;
	}
	
	// Update is called once per frame
	void Update () {	
	}

	public void beginDrag() {
		position = gameObject.transform.position;
		dragging = true;
	}

	public void drag() {
		transform.position = Input.mousePosition;
		//SEAGULLS.Play(0);
	}

	public void drop () {
		if(!collision) {
			gameObject.transform.position = Input.mousePosition;
		}
		//llamar funcion
		manager.comprobar(id);
		//Debug.Log("estado playa "+ Items.playa);
		dragging = false;
		//Debug.Log(gameObject.transform.position);
	}
	public void resetPosicion(){
		transform.position = positionInicial;
	}
	public void setPositionTrash(Vector3 trash){
		transform.position = trash;
		m_Rigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
	}
	

}
