﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(AudioSource))]
public class ItemsArrastrarSilavas : MonoBehaviour {

	public bool dragging = false;
	public bool collision = false;
	private Vector3 position;
	public ArrastrarSilavas manager;
	public int id;
	private Vector3 positionInicial;
	public ActivarAudio audio;

	void Start () {		
		positionInicial = transform.position;
	}
	
	// Update is called once per frame
	void Update () {		
	}

	public void beginDrag() {
		position = gameObject.transform.position;
		dragging = true;
		audio.play();
	}

	public void drag() {
		transform.position = Input.mousePosition;

		//SEAGULLS.Play(0);
	}

	public void drop () {
		if(!collision) {
			gameObject.transform.position = Input.mousePosition;
		}
		//llamar funcion
		if(manager.comprobar(id)){
			gameObject.transform.position = manager.getPositionAux();
		}else{
			resetPosition();
		}
		//Debug.Log("estado playa "+ Items.playa);
		dragging = false;
		//Debug.Log(gameObject.transform.position);
	}
	public void resetPosition(){
	Debug.Log("resetposition");
		transform.position = positionInicial;
	}
	

}
