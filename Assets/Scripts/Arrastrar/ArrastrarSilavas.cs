﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using UnityEngine.SceneManagement;

public class ArrastrarSilavas : MonoBehaviour {

	// Use this for initialization
	private string figura;
	private int idSilaba;
	
	private Vector3 posicionAux;
	private int imagen;
	public GameObject[] silavasLLA = new GameObject[4];
	public GameObject[] silavasLLE = new GameObject[4];
	public GameObject[] silavasLLI = new GameObject[2];
	public GameObject[] silavasLLO = new GameObject[4];
	public GameObject[] silavasLLU = new GameObject[1];

	public ItemsArrastrarSilavas silavasItemLLA;
	public ItemsArrastrarSilavas silavasItemLLE;
	public ItemsArrastrarSilavas silavasItemLLI;
	public ItemsArrastrarSilavas silavasItemLLO;
	public ItemsArrastrarSilavas silavasItemLLU;
	
	private bool estadoTrash;
	public int repetir;
	private int contadorVueltas;
	public int[] posicionesIndicesRonda;
	private int[] ordenImagenes = new int[2];
	public ControllerEncabezado encabezado;
	public Conexion conexion;

  void Awake(){
  	posicionesIndicesRonda = new int[repetir];
  }
	void Start () {
		for(int i = 0; i<repetir; i++){
			posicionesIndicesRonda[i] = -1;
		}
		asignarIndicesRonda();
		asignarIndicesImagenes();
		iniciarElementos();
		contadorVueltas = 0;
	}
	void Update (){
	}
	private void iniciarElementos(){
		this.estadoTrash = false;
		
		if(posicionesIndicesRonda[contadorVueltas]>4){
			imagen = ordenImagenes[0];		
		}else{
			imagen = ordenImagenes[1];	
		}
		escogerSilavaCorrecta();
	}
	public void resetEscena(){
		
		switch(posicionesIndicesRonda[contadorVueltas]){
			case 0:
				silavasLLA[imagen].SetActive(false);
				silavasItemLLA.resetPosition();
				break;
			case 1:
				silavasLLE[imagen].SetActive(false);
				silavasItemLLE.resetPosition();
				break;
			case 2:
				silavasLLI[0].SetActive(false);
				silavasItemLLI.resetPosition();
				break;
			case 3:
				silavasLLO[imagen].SetActive(false);
				silavasItemLLO.resetPosition();
				break;
			case 4:
				silavasLLU[0].SetActive(false);
				silavasItemLLU.resetPosition();
				break;
			case 5:
				silavasLLA[imagen].SetActive(false);
				silavasItemLLA.resetPosition();
				break;
			case 6:
				silavasLLE[imagen].SetActive(false);
				silavasItemLLE.resetPosition();
				break;
			case 7:
				silavasLLI[1].SetActive(false);
				silavasItemLLI.resetPosition();
				break;
			case 8:
				silavasLLO[imagen].SetActive(false);
				silavasItemLLO.resetPosition();
				break;
		}
		contadorVueltas++;
		if(repetir==contadorVueltas){
			StartCoroutine("cargarCompletada");
		}else{
			iniciarElementos();
		}
	}
	IEnumerator cargarCompletada(){
		yield return new WaitForSeconds(2);
		SceneManager.LoadScene("Completada");
	}
	// Update is called once per frame
	public void escogerSilavaCorrecta(){
		switch(posicionesIndicesRonda[contadorVueltas]){
			case 0:
				silavasLLA[imagen].SetActive(true);
				break;
			case 1:
				silavasLLE[imagen].SetActive(true);
				break;
			case 2:
				silavasLLI[0].SetActive(true);
				break;
			case 3:
				silavasLLO[imagen].SetActive(true);
				break;
				case 4:
				silavasLLU[0].SetActive(true);
				break;
			case 5:
				silavasLLA[imagen].SetActive(true);
				break;
			case 6:
				silavasLLE[imagen].SetActive(true);
				break;
			case 7:
				silavasLLI[1].SetActive(true);
				break;
			case 8:
				silavasLLO[imagen].SetActive(true);
				break;
			}
	}
	
	
	public void setPositionAux(Vector3 position){
		posicionAux = position;
	}
	public void setEstadoTrash(bool estado){
		this.estadoTrash = estado;
	}
	public Vector3 getPositionAux(){
		return posicionAux;
	}
	public bool comprobar(int id){
		if((id==posicionesIndicesRonda[contadorVueltas]||(posicionesIndicesRonda[contadorVueltas] - 5) == id)&&this.estadoTrash){
			ValidacionController.cargarResultadoImage(true);
			StartCoroutine("realizarCargaDatos");
			return true;
		}else{
			encabezado.disminuirVidas();
			ValidacionController.cargarResultadoImage(false);
			return false;
		}
	}
	IEnumerator realizarCargaDatos(){
		conexion.cargarDatos();
		yield return new WaitForSeconds(2);
		resetEscena();
		ProjectVars.Instance.numero++;
	}
	private void asignarIndicesRonda(){
		for(int i = 0; i<this.repetir;i++){
			posicionesIndicesRonda[i] = generadorPosicionesRandom();
			//Debug.Log(posicionesIndicesRonda[i]);
		}
	}
	private int generadorPosicionesRandom(){
		int aux = Random.Range(0, repetir);
		bool estado = encontrarIgualPosicion(aux);
		if(estado){
			return aux;
		}else{
			return	generadorPosicionesRandom();
		}
	}
	
	private bool encontrarIgualPosicion(int numeroRandom){
		bool estado = true;
		for(int i = 0;i<this.repetir;i++){
			//cambio por numero random posicionesIndicesRonda[i], sino entra a bucle infinito
			if((numeroRandom==posicionesIndicesRonda[i]) ){
				estado = false;
				break;
			}
		}
		return estado;
	}


	private void asignarIndicesImagenes(){
		for(int i = 0; i<2;i++){
			ordenImagenes[i] = -1;
			ordenImagenes[i] = generadorPosicionesImagenesRandom();
			//Debug.Log(posicionesIndicesRonda[i]);
		}
	}
	private int generadorPosicionesImagenesRandom(){
		int aux = Random.Range(0, 4);
		bool estado = encontrarIgualPosicionImagen(aux);
		if(estado){
			Debug.Log("random posicion "+aux);
			return aux;
		}else{
			return	generadorPosicionesImagenesRandom();
		}
	}
	
	private bool encontrarIgualPosicionImagen(int numeroRandom){
		bool estado = true;
		for(int i = 0;i<2;i++){
			//cambio por numero random posicionesIndicesRonda[i], sino entra a bucle infinito
			if((numeroRandom==ordenImagenes[i]) ){
				estado = false;
				break;
			}
		}
		return estado;
	}

}
